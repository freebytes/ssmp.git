# SpringBoot+Shiro+MyBatisPlus 企业级项目搭建

#### 介绍
使用SpringBoot+Shiro+MyBatisPlus三大框架搭建前后端分离的web项目，
具有基本的权限、角色、用户操作以及sql建表语句，自定义登录、退出过滤器，使用缓存鉴权。
集成了swagger工具和切面日志功能，
满足普通公司的企业级项目的基本应用。

#### 原创
出自明月工作室 http://www.freebytes.net

#### 软件架构
SpringBoot+Shiro+MyBatisPlus

#### 安装工具

1.  maven
2.  jdk

#### 使用说明

1.  使用sql/ssmp.sql文件，创建数据库和数据表、初始数据
2.  修改application.properties文件的数据库相关配置
3.  启动类Application.java
4.  浏览器输入localhost:8080/login.html访问登录页面，默认用户名和密码为 admin admin
5.  输入http://localhost:8080/swagger-ui.html，访问swagger

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request



