package net.freebytes.ssmp;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;


@EnableAutoConfiguration
@ComponentScan
@MapperScan(basePackages = {"net.freebytes.ssmp.mapper"}) //扫描Mapper
/**
 *启动类
 *
 *@author 千里明月
 *@date 2021/7/20
 **/
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class);
    }
}
