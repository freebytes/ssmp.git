package net.freebytes.ssmp.aspect;

/**
 * 行为枚举
 *
 * @author 千里明月
 * @date 2021/7/20
 **/
public enum BusinessStatus {

    /**
     * 默认值
     */
    DEFAULT,

    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,

}
