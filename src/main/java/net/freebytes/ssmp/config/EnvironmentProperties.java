package net.freebytes.ssmp.config;

/**
 * 存储项目设置的环境变量
 *
 * @author 千里明月
 * @date 2021/7/20
 **/
public class EnvironmentProperties {
    /**
     * 外部静态资源的绝对路径
     */
    private static String uploadPath;
    /**
     * 存放前端文件的地址
     */
    private static String webFrontPath;


    public static String getWebFrontPath() {
        return webFrontPath;
    }

    public void setWebFrontPath(String webFrontPath) {
        EnvironmentProperties.webFrontPath = webFrontPath;
    }

    public static String getUploadPath() {
        return uploadPath;
    }

    public void setUploadPath(String uploadPath) {
        EnvironmentProperties.uploadPath = uploadPath;
    }

}
