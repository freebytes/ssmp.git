package net.freebytes.ssmp.config;

import net.freebytes.ssmp.shiro.CusAuthenticationFilter;
import net.freebytes.ssmp.shiro.CusLogoutFilter;
import net.freebytes.ssmp.shiro.CustomRealm;
import net.freebytes.ssmp.shiro.CustomSessionManager;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.session.mgt.SessionManager;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;
import java.util.LinkedHashMap;
import java.util.Map;

/**
*
*@author 千里明月
*@date 2021/7/20
**/
@Configuration
public class ShiroConfig {
    @Value("${default.login:false}")
    private boolean defaultLogin = false;

    @Bean
    @ConditionalOnMissingBean
    public DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator() {
        DefaultAdvisorAutoProxyCreator defaultAap = new DefaultAdvisorAutoProxyCreator();
        defaultAap.setProxyTargetClass(true);
        return defaultAap;
    }


    //@Bean

    /**
     * 自定义的sessionManager
     *
     * @author 千里明月
     * @date 2021/7/20
     **/
    public SessionManager sessionManager() {
        CustomSessionManager mySessionManager = new CustomSessionManager();
        mySessionManager.setGlobalSessionTimeout(86400000L);
        //去除浏览器地址栏中url中JSESSIONID参数
        mySessionManager.setSessionIdUrlRewritingEnabled(false);
        return mySessionManager;
    }


    /**
     * 将自己的验证方式加入容器
     */
    @Bean
    public CustomRealm customRealm() {
        CustomRealm customRealm = new CustomRealm();
        //设置加密算法
        customRealm.setCredentialsMatcher(credentialsMatcher());
        return customRealm;
    }

    /**
     * 权限管理，配置主要是Realm的管理认证
     *
     * @author 千里明月
     * @date 2021/7/20
     **/
    @Bean
    public SecurityManager securityManager() {
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        securityManager.setRealm(customRealm());
        return securityManager;
    }

    @Bean("credentialsMatcher")
    public HashedCredentialsMatcher credentialsMatcher() {
        HashedCredentialsMatcher credentialsMatcher = new HashedCredentialsMatcher();
        //加密算法的名字，也可以设置MD5等其他加密算法名字
        credentialsMatcher.setHashAlgorithmName("SHA-256");
        //加密次数
        credentialsMatcher.setHashIterations(20);
        //加密为哈希
        credentialsMatcher.setStoredCredentialsHexEncoded(true);
        return credentialsMatcher;
    }

    /**
     * Filter工厂，设置对应的过滤条件和跳转条件
     *
     * @author 千里明月
     * @date 2021/7/20
     **/
    @Bean(name = "shiroFilter")
    public ShiroFilterFactoryBean shiroFilterFactoryBean(SecurityManager securityManager) {
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        shiroFilterFactoryBean.setSecurityManager(securityManager);

        //配置自定义认证过滤器和退出登录过滤器
        Map<String, Filter> filters = new LinkedHashMap();
        filters.put("cauthc", new CusAuthenticationFilter(defaultLogin));
        filters.put("clogout", new CusLogoutFilter());
        shiroFilterFactoryBean.setFilters(filters);

        // 拦截配置
        LinkedHashMap<String, String> filterChainDefinitionMap = new LinkedHashMap<>();
        filterChainDefinitionMap.put("/rest/ssmp/login", "anon");
        //为一个路径绑定自定义的退出登录过滤器
        filterChainDefinitionMap.put("/rest/ssmp/logout", "clogout");
        // 不需要拦截的访问
        filterChainDefinitionMap.put("/common/**", "anon");

        // 对静态资源设置匿名访问
        filterChainDefinitionMap.put("/", "anon");
        filterChainDefinitionMap.put("/login.html", "anon");
        filterChainDefinitionMap.put("/css/**", "anon");
        filterChainDefinitionMap.put("/img/**", "anon");
        filterChainDefinitionMap.put("/js/**", "anon");
        filterChainDefinitionMap.put("/fonts/**", "anon");
        filterChainDefinitionMap.put("/favicon.ico", "anon");
        filterChainDefinitionMap.put("/settings.js", "anon");
        filterChainDefinitionMap.put("/*.js.gz", "anon");
        //swagger接口权限 开放
//        filterChainDefinitionMap.put("/swagger-ui.html", "anon");
//        filterChainDefinitionMap.put("/swagger/**", "anon");
//        filterChainDefinitionMap.put("/webjars/**", "anon");
//        filterChainDefinitionMap.put("/swagger-resources/**", "anon");
//        filterChainDefinitionMap.put("/v2/**", "anon");
//        filterChainDefinitionMap.put("/doc.html", "anon");

        // 所有url都必须认证通过才可以访问，这里绑定自定义认证过滤器
        filterChainDefinitionMap.put("/**", "cauthc");

        shiroFilterFactoryBean.setFilterChainDefinitionMap(filterChainDefinitionMap);

        return shiroFilterFactoryBean;
    }


    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(SecurityManager securityManager) {
        AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
        authorizationAttributeSourceAdvisor.setSecurityManager(securityManager);
        return authorizationAttributeSourceAdvisor;
    }
}