package net.freebytes.ssmp.config;

import io.swagger.annotations.Api;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@ConditionalOnExpression("${swagger.enabled: true}")
/**
*
*@author 千里明月
*@date 2021/7/20
**/
public class SwaggerConfig {
    @Bean
    public Docket api() {
        Docket docket = new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(getApiInfo())
                .useDefaultResponseMessages(false)
                .forCodeGeneration(false)
                // 选择那些路径和api会生成document
                .select()
                // 对所有api进行监控
                .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
                //对"com.cgs.dcp"包下全部进行扫描
//                .apis(RequestHandlerSelectors.basePackage("com.cgs.dcp"))
                // 对所有路径进行监控
                .paths(PathSelectors.regex("/.*"))
//                .paths(PathSelectors.regex("/demo.*"))
                .build();
        return docket;
    }

    @Deprecated
    private ApiInfo getApiInfo() {
        // 大标题
        ApiInfo apiInfo = new ApiInfo(" SpringBoot+Shiro+MyBatisPlus框架集成",
                // 简单的描述
                " 原创于明月工作室",
                // 版本
                "1.0.1",
                "http://www.freebytes.net",
                // 作者
                "明月工作室",
                // 链接显示文字
                "明月工作室",
                // 网站链接
                "http://www.freebytes.net"
        );
        return apiInfo;
    }

}
