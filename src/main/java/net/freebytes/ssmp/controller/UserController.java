package net.freebytes.ssmp.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import net.freebytes.ssmp.entity.User;
import net.freebytes.ssmp.service.UserService;
import net.freebytes.ssmp.util.RespBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户管理
 *
 * @author 千里明月
 * @date 2021/7/23
 **/
@Api(tags = "用户管理")
@RestController
@RequestMapping("/rest/ssmp/user")
public class UserController {
    @Autowired
    private UserService userService;

    @ApiOperation(value = "获取用户详情-不包括密码")
    @GetMapping("/get/{id}")
    public RespBean get(@PathVariable("id") String id) {
        User user = userService.cacheUserDetail(id);
        return RespBean.ok(user);
    }
}
