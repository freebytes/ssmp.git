package net.freebytes.ssmp.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;
import java.util.Objects;

/**
 * @author 千里明月
 * @date 2021/7/20
 **/
@ApiModel("角色单页")
public class Role extends BaseEntity {
    @ApiModelProperty("角色名称")
    private String name;
    @ApiModelProperty("角色对应权限集合")
    @TableField(exist = false)
    private List<Permission> permissions;
    @ApiModelProperty("备注")
    private String des;

    @Override
    public String toString() {
        return "Role{" +
                "id='" + getId() + '\'' +
                ", createTime='" + getCreateTime() + '\'' +
                ", name='" + name + '\'' +
                ", permissions=" + permissions +
                ", des='" + des + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<Permission> permissions) {
        this.permissions = permissions;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Role)) {
            return false;
        }
        Role that = (Role) o;
        return getId().equals(that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName());
    }
}