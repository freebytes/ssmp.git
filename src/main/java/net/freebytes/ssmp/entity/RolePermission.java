package net.freebytes.ssmp.entity;

import java.io.Serializable;

/**
*
*@author 千里明月
*@date 2021/7/20
**/
public class RolePermission implements Serializable {
    protected static final long serialVersionUID = 1L;
    private String roleId;
    private String permissionId;

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(String permissionId) {
        this.permissionId = permissionId;
    }
}
