package net.freebytes.ssmp.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

/**
 * @author 千里明月
 * @date 2021/7/20
 **/
@ApiModel("用户")
public class User extends BaseEntity {
    @ApiModelProperty("用户名")
    private String username;
    @ApiModelProperty("密码")
    private String password;
    @ApiModelProperty("姓名")
    private String name;
    @ApiModelProperty("盐")
    private String salt;
    @ApiModelProperty("用户对应的角色集合")
    @TableField(exist = false)
    public List<Role> roles;


    @ApiModelProperty("用户对应的权限集合")
    @TableField(exist = false)
    public List<Permission> permissions;

    @Override
    public String toString() {
        return "User{" +
                "id='" + getId() + '\'' +
                ", createTime='" + getCreateTime() + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", salt='" + salt + '\'' +
                ", roles=" + roles +
                ", permissions=" + permissions +
                '}';
    }

    public List<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<Permission> permissions) {
        this.permissions = permissions;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }
}