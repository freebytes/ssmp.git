package net.freebytes.ssmp.exception;

import org.apache.shiro.authz.AuthorizationException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 自定义异常类
 *
 * @author 千里明月
 * @date 2021/7/20
 **/
@ControllerAdvice
public class CusExceptionHandler {

    @ExceptionHandler
    @ResponseBody
    public String errorHandler(AuthorizationException e) {
        return "没有通过权限验证！";
    }

}