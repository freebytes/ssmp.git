package net.freebytes.ssmp.exception;

/**
 * 用户异常类
 *
 * @author 千里明月
 * @date 2021/7/23
 **/
public class UserException extends RuntimeException {
    public UserException() {
        super();
    }

    public UserException(String message) {
        super(message);
    }

    public UserException(String message, Throwable cause) {
        super(message, cause);
    }

}
