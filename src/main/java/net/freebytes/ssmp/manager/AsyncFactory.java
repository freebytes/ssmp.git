package net.freebytes.ssmp.manager;

import net.freebytes.ssmp.entity.OperLog;
import net.freebytes.ssmp.service.OperLogService;
import net.freebytes.ssmp.util.IpUtils;

import java.util.TimerTask;

/**
 * 异步工厂（产生任务用）
 *
 * @author 千里明月
 * @date 2021/7/20
 **/
public class AsyncFactory {

    /**
     * 操作日志记录
     *
     * @param operLog 操作日志信息
     * @return 任务task
     */
    public static TimerTask recordOper(final OperLog operLog) {
        return new TimerTask() {
            @Override
            public void run() {
                // 远程查询操作地点
                try {
                    operLog.setOperLocation(IpUtils.getRealAddressByIp(operLog.getOperIp()));
                } catch (Exception e) {

                }
                SpringContext.getBean(OperLogService.class).insertOperlog(operLog);
            }
        };
    }

}
