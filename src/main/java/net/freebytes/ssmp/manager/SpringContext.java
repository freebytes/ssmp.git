package net.freebytes.ssmp.manager;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.stereotype.Component;

/**
*
*@author 千里明月
*@date 2021/7/20
**/
@Component
public class SpringContext implements BeanFactoryPostProcessor {

    /**
     * Spring应用上下文环境
     */
    private static ConfigurableListableBeanFactory beanFactory;

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        SpringContext.beanFactory = beanFactory;
    }

    public static <T> T getBean(Class<T> clz) {
        return beanFactory.getBean(clz);
    }

    public static <T> T getBean(String name) {
        return (T)beanFactory.getBean(name);
    }
}
