package net.freebytes.ssmp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import net.freebytes.ssmp.entity.Permission;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 权限
 *
 * @author 千里明月
 * @date 2021/7/19
 **/
public interface PermissionMapper extends BaseMapper<Permission> {


    /**
     * 通过角色id找权限
     *
     * @param roleIds
     * @return java.util.List<net.freebytes.ssmp.entity.Permission>
     * @author 千里明月
     * @date 2021/7/19
     **/
    List<Permission> getPermissionByRoleId(@Param("roleIds") List roleIds);

    /**
     * 通过用户id找权限
     *
     * @param uid
     * @return java.util.List<net.freebytes.ssmp.entity.Permission>
     * @author 千里明月
     * @date 2021/7/19
     **/
    @Select("SELECT p.* FROM permission p,role_permission rp, user_role ur  WHERE ur.user_id=#{uid} AND ur.role_id=rp.role_id and p.id=rp.permission_id")
    List<Permission> allPermissionByUid(String uid);

}
