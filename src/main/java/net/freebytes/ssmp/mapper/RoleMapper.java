package net.freebytes.ssmp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import net.freebytes.ssmp.entity.Role;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author 千里明月
 * @date 2021/7/20
 **/
public interface RoleMapper extends BaseMapper<Role> {

    /**
     * 根据用户id获取角色列表
     *
     * @param userId
     * @return java.util.List<net.freebytes.ssmp.entity.Role>
     * @author 千里明月
     * @date 2021/7/20
     **/
    @Select("SELECT role.id, role.name, role.des from role left join user_role  on role.id=user_role.role_id where user_role.user_id=#{userId};")
    List<Role> getRoleByUserId(@Param("userId") String userId);
}
