package net.freebytes.ssmp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import net.freebytes.ssmp.entity.RolePermission;

/**
*
*@author 千里明月
*@date 2021/7/20
**/
public interface RolePermMapper extends BaseMapper<RolePermission> {
}
