package net.freebytes.ssmp.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import net.freebytes.ssmp.entity.OperLog;
import net.freebytes.ssmp.mapper.OperLogMapper;
import org.springframework.stereotype.Service;

/**
 * 日志
 *
 * @author 千里明月
 * @date 2021/7/20
 **/
@Service
public class OperLogService extends ServiceImpl<OperLogMapper, OperLog> {

    /**
     * 创建系统操作日志
     *
     * @param operLog 操作日志对象
     */
    public void insertOperlog(OperLog operLog) {
        super.save(operLog);
    }

}