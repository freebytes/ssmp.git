package net.freebytes.ssmp.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import net.freebytes.ssmp.entity.Role;
import net.freebytes.ssmp.entity.RolePermission;
import net.freebytes.ssmp.mapper.PermissionMapper;
import net.freebytes.ssmp.mapper.RolePermMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author 千里明月
 * @date 2021/7/20
 **/
@Service
public class RolePermService extends ServiceImpl<RolePermMapper, RolePermission> {

    @Autowired
    private PermissionMapper permMapper;

    public boolean deleteByPerm(List<String> permIds) {
        return super.remove(lambdaQuery().eq(RolePermission::getPermissionId, permIds));
    }

    public boolean deleteByRole(List<String> roleIds) {
        return super.remove(lambdaQuery().eq(RolePermission::getRoleId, roleIds));
    }

    public boolean delete(String roleId, String permId) {
        HashMap<String, Object> map = new HashMap<>(4);
        map.put("role_id", roleId);
        map.put("perm_id", permId);
        return super.removeByMap(map);
    }

    public List<String> getPermIdByRole(String roleId) {
        List<RolePermission> rolePermissions = super.list(new QueryWrapper<RolePermission>().lambda().eq(RolePermission::getRoleId, roleId));
        return rolePermissions.stream().map(RolePermission::getPermissionId).collect(Collectors.toList());
    }



    public Role getRoleWithPerm(String roleId) {
        return null;
    }


}
