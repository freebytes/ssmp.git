package net.freebytes.ssmp.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import net.freebytes.ssmp.entity.UserRole;
import net.freebytes.ssmp.mapper.UserRoleMapper;

/**
*
*@author 千里明月
*@date 2021/7/20
**/
public class UserRoleService extends ServiceImpl<UserRoleMapper, UserRole> {
}
